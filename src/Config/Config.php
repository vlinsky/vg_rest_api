<?php
namespace App\Config;

class Config
{
    public static function getRedisConnectionString() : string
    {
        return $_ENV['REDIS_URL'];
    }
    
    public static function getRedisSessionTimeout() : string
    {
        return $_ENV['REDIS_SESSION_TIMEOUT'];
    }
    
    public static function getRedisSessionNamespace() : string
    {
        return $_ENV['REDIS_SESSION_NEMASPACE'];
    }
}