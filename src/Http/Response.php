<?php
namespace App\Http;

use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

/**
 * Response class
 *
 * exdends HttpFoundationResponse class. 
 * By defualt use headers 'Content-Type' => 'application/json' and 'Access-Control-Allow-Origin' => '*'
 */
class Response extends HttpFoundationResponse
{
    private $defaultHeades = [
        'Content-Type' => 'application/json',
        'Access-Control-Allow-Origin' => '*',
    ];
    
    public function __construct($content = '', int $status = 200, array $headers = array())
    {
        $mHeaders = array_merge($headers, $this->defaultHeades);
        parent::__construct(json_encode($content), $status, $mHeaders);
    }
}