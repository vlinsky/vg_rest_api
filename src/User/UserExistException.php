<?php
namespace App\User;

/**
 * UserExistException class
 *
 * throws when user with gived email exist during registraion
 */
class UserExistException extends UserException
{
    
}