<?php
namespace App\User;

/**
 * User exception class
 * 
 * throws in User class
 */
class UserException extends \Exception
{
    
}