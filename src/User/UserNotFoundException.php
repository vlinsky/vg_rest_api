<?php
namespace App\User;

/**
 * UserNotFoundException
 * 
 * throws then user not found in DB
 */
class UserNotFoundException extends  UserException
{
    
}