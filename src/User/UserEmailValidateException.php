<?php
namespace App\User;

/**
 * UserEmailValidateException
 * 
 * throws when email validation fails
 */
class UserEmailValidateException extends UserValidationException
{
    
}