<?php
namespace App\User;

/**
 * UserPasswordValidateException class
 * 
 * throws when user password validation fails
 */
class UserPasswordValidateException extends UserValidationException
{
    
}