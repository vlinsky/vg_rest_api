<?php
namespace App\User;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints\Email as EmailConstraint;

/**
 * UserFieldValidator
 * 
 */
class UserFieldValidator
{
    private $validator;
    
    /**
     * 
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }
    
    /**
     * Validate user added email
     * 
     * @param string $email
     * @throws UserEmailValidateException - if validation fails
     * @return bool - true if no errors
     */
    public function validateEmail(string $email): bool
    {
        $emailConstraint = new EmailConstraint();
        $emailConstraint = array(
            new \Symfony\Component\Validator\Constraints\Email(),
            new \Symfony\Component\Validator\Constraints\NotBlank()
        );
        
        $errors = $this->validator->validate($email, $emailConstraint);
        
        if (count($errors) > 0) {
            throw new UserEmailValidateException($errors[0]->getMessage());
        }
        
        return true;
    }
    
    /**
     * Validate user added password
     *
     * @param string $passwd
     * @throws UserPasswordValidateException - if validation fails
     * @return bool - true if no errors
     */
    public function validatePassword(string $passwd): bool
    {
        $passwdConstrains = array(
            new \Symfony\Component\Validator\Constraints\Length(['min'=>6]),
            new \Symfony\Component\Validator\Constraints\NotBlank()
        );
        
        $errors = $this->validator->validate($passwd, $passwdConstrains);
        
        if (count($errors) > 0) {
            throw new UserPasswordValidateException($errors[0]->getMessage());
        }
        
        return true;
    }
}