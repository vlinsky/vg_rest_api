<?php
namespace App\User;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Invitations\Invitation;
use App\Invitations\InvitationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use App\Entity\User as User;

/**
 * UserManager class
 * 
 * implement abstract level to work with UserManager Enity object
 */
class UserManager
{   
    private $em;
    private $encoder;
    private $validator;
    private $repository;
    
    /**
     * 
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $encoder
     * @param ValidatorInterface $validator
     */
    public function __construct(EntityManagerInterface $em, 
                                UserPasswordEncoderInterface $encoder = null,
                                ValidatorInterface $validator = null)
    {
        $this->em = $em;
        $this->encoder = $encoder;
        $this->validator = $validator;
        $this->repository = $em->getRepository(User::class);
    }
    
    
    /**
     * Validate user input data
     * 
     * @param string $email
     * @param string $passwd
     */
    public function validateData(string $email, string $passwd): void
    {
        $userValidator = new UserFieldValidator($this->validator);
        $userValidator->validateEmail($email);
        $userValidator->validatePassword($passwd);
    }
    
    /**
     * Add new user to DB
     * 
     * @param string $email
     * @param string $passwd
     * @param string $invitationCode
     * @throws InvitationException
     * @throws UserValidationException
     * @throws UserException
     * @return User|NULL
     */
    public function add(string $email, string $passwd, string $invitationCode = null): ?User
    {
        try {
            $invitationId = null;
            
            $this->validateData($email, $passwd);
            
            if ($this->isExist($email)) {
                throw new UserExistException();
            }
            
            $user = new User();
            
            $user->setEmail($email);
            $user->setPasswd($this->encoder->encodePassword($user, $passwd));
            $user->setDateAdded(new \DateTime('now'));
            
            $invitation = new Invitation($this->em);
            
            if (isset($invitationCode)) {
                $invitationEntity = $invitation->getByCode($invitationCode);
                
                if (null === $invitationEntity) {
                    throw new InvitationException('Invalid invitation code');
                } elseif (null !== $invitationEntity->getDateUsed()) {
                    throw new InvitationException('Invitation is used');
                }
                $invitationId = $invitationEntity->getId();
                $invitationEntity->setDateUsed(new \DateTime('now'));
                
                $this->em->persist($invitationEntity);
                $this->em->flush();
            }
            
            $user->setInviteId($invitationId);
            
            $this->em->persist($user);
            $this->em->flush();
            
            return $user;
        } catch (UserValidationException $e) {
            throw new UserValidationException($e->getMessage());
        } catch (InvitationException $e) {
            throw new InvitationException($e->getMessage());
        } catch(UserExistException $e) {
            throw new UserException("UserManager with email ".$email." already exist");
        } catch (\Exception $e) {
            throw new UserException("Error adding new user");
        }
    }
    
    /**
     * Check if user with gived email and password exist in DB
     * 
     * @param string $email
     * @param string $passwd
     * @throws UserNotFoundException
     * @throws UserException
     * @return User|NULL
     */
    public function checkAuth(string $email, string $passwd): ?User
    {
        try {
            
            $user = $this->repository->findOneBy(['email' => $email]);
            
            if (!$user || !$this->encoder->isPasswordValid($user, $passwd)) {
                throw new UserNotFoundException("UserManager not found. Please check email or password");
            }
            
            return $user;
        } catch (UserNotFoundException $e) {
            throw new UserException($e->getMessage());
        } catch (\Exception $e) {
            throw new UserException("Error logging. Try later.");
        }
    }
    
    /**
     * Check if email exist in DB
     * 
     * @param string $email
     * @throws UserException
     * @return User|NULL
     */
    public function isExist(string $email): ?User
    {
        try { 
            $user = $this->repository->findOneBy(['email' => $email]);
            
            return $user;
        } catch (UserNotFoundException $e) {
            return null;
        } catch (\Exception $e) {
            throw new UserException("Error logging. Try later.");
        }
    }
    
    /**
     * Update existing user in DB
     * 
     * @return bool
     */
    public function update(): bool
    {
        
    } 
}