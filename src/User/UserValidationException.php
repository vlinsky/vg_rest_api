<?php
namespace App\User;

/**
 * UserValidationException class
 * 
 * throws when validation error occurs
 */
class UserValidationException extends UserException
{
    
}