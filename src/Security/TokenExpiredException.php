<?php
namespace App\Security;

/**
 * TokenExpiredException exception
 *
 * throws then user auth token is expired
 */
class TokenExpiredException extends \Exception
{
    
}