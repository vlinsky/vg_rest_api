<?php
namespace App\Invitations;

use App\Entity\Invitation;
use Doctrine\ORM\EntityManagerInterface;

/**
 * InvitationList class
 */
class InvitationList implements InvitationListInterface
{
    private $em;
    private $repository;
    
    /**
     * 
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository(Invitation::class);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \App\Invitations\InvitationListInterface::getByUserId()
     */
    public function getByUserId($userId): array
    {
        try {
            $qb = $this->em->createQueryBuilder();
            
            $qb->select('i')
                    ->from('App\Entity\Invitation', 'i')
                    ->where('i.userId = :userId')
                    ->setParameter('userId', $userId)
                    ->orderBy('i.dateUsed', 'ASC');
            
            $query = $qb->getQuery();
            
            $invitations = $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            
            return $invitations;
        } catch (\Exception $e) {
            throw new InvitationListException('Error geting invite codes');
        }
    }
    
    /**
     * Get user short statistic for invitation codes
     * 
     * get count used and unused invitaion codes
     * 
     * @param mixed $userId
     * @return array
     */
    public function getShortStatistic($userId): array
    {
        try {
            //SELECT SUM(ISNULL(date_used)) as invited, SUM(!ISNULL(date_used)) as registered FROM `invitation` WHERE user_id=33
            
            $statistic = [
                'registered' => $this->getUsedInvitationCount($userId),
                'invited' => $this->getNotUsedInvitationCount($userId)
                
            ];
            
            return $statistic;
        } catch (\Exception $e) {
            throw new InvitationListException('Error geting invite codes statistic');
        }
    }
    
    /**
     * Get not used invitation count
     * 
     * @param mixed $userId
     * @return int
     */
    private function getNotUsedInvitationCount($userId): int
    {
        try {
            $qb = $this->em->createQueryBuilder();
            
            $qb->select('COUNT(i.id) as registered')
                    ->from('App\Entity\Invitation', 'i')
                    ->where($qb->expr()->andX(
                        $qb->expr()->eq('i.userId', '?1'),
                        $qb->expr()->isNull('i.dateUsed')))
                    ->setParameter('1', $userId);
                
            $query = $qb->getQuery();
                
            $statistic = $query->getResult()[0]['registered'];
            
            return $statistic;
        } catch (\Exception $e) {
            throw new InvitationListException('Error geting invite codes count');
        }
    }
    
    /**
     * Get used invitation count
     *
     * @param mixed $userId
     * @return int
     */
    private function getUsedInvitationCount($userId): int
    {
        try {
            $qb = $this->em->createQueryBuilder();
            
            $qb->select('COUNT(i.id) as invited')
                    ->from('App\Entity\Invitation', 'i')
                    ->where($qb->expr()->andX(
                        $qb->expr()->eq('i.userId', '?1'),
                        $qb->expr()->isNotNull('i.dateUsed')))
                    ->setParameter('1', $userId);
                
             $query = $qb->getQuery();
                
             $invited = $query->getResult()[0]['invited'];
                
             return $invited;
        } catch (\Exception $e) {
            throw new InvitationListException('Error geting invite codes count');
        }
    }
}