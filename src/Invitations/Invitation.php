<?php
namespace App\Invitations;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Invitation as InvitationEntity;

/**
 * Invitation class
 *
 */
class Invitation implements InvitationInterface
{   
    private $em;
    private $repository;
    
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository(InvitationEntity::class);
    }
    
    /**
     * Add new invite code into DB
     * 
     * @param string $code
     * @param int $userId
     */
    public function add(string $code, int $userId): ?InvitationEntity
    {
        try {
            $invitationEntity = new InvitationEntity();
            
            $invitationEntity->setCode($code);
            $invitationEntity->setUserId($userId);
            $invitationEntity->setDateAdded(new \DateTime('now'));
            
            $this->em->persist($invitationEntity);
            $this->em->flush();
            
            return $invitationEntity;
        } catch (UniqueConstraintViolationException $e) {
            return null;
        } catch (\Exception $e) {
            throw new InvitationException('Error adding new invite code');
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see \App\Invitations\InvitationInterface::getByCode()
     * @throws InvitationException
     */
    public function getByCode(string $code): ?InvitationEntity
    {
        try {
            $invitation = $this->repository->findOneBy(['code' => $code]);
            
            return $invitation;
        } catch (\Doctrine\ORM\NoResultException $e){
            throw new InvitationException("No invitation found by code");
        } catch (\Exception $e) {
            throw new InvitationException("Error while getting invitation");
        }
    }
}