<?php
namespace App\Invitations;

use App\Entity\Invitation;

/**
 * Invitation interface
 * 
 * each invitation should implements this interface
 */
interface InvitationInterface
{   
    /**
     * Get invitation by code
     * 
     * @return Invitation|NULL
     */
    public function getByCode(string $code): ?Invitation;
}