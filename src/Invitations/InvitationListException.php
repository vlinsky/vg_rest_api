<?php
namespace App\Invitations;

/**
 * InvitationListException
 * 
 * throws when error in InvitationList occurs
 */
class InvitationListException extends InvitationException
{
    
}