<?php
namespace App\Invitations\Generator;

use Doctrine\ORM\EntityManagerInterface;
use App\Invitations\Invitation;
use App\Invitations\InvitationException;

/**
 * InvitationGenerator
 *
 * generate invitation codes
 */
class InvitationGenerator implements InvitationGeneratorInterface
{
    private $em;
    private $userId;
    private $invitations = [];
    
    /**
     * Default code length
     */
    private const DEFAULT_CODE_LENGTH = 10;
    
    /**
     * Max count for generate invate codes
     */
    private const MAX_GENERATED_CODES_COUNT = 10;
    
    /**
     * Max limit for looking unique code
     */
    private const MAX_INCREASE_LIMIT = 50;
    
    /**
     * 
     * @param int $userId
     * @param EntityManagerInterface $em
     */
    public function __construct(int $userId, EntityManagerInterface $em)
    {
        $this->userId = $userId;
        $this->em = $em;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \App\Invitations\Generator\InvitationGeneratorInterface::getUniqueCode()
     */
    public function getUniqueCode(): string
    {
        return  strtoupper(substr(md5(uniqid(null, true)), rand(0, 10), self::DEFAULT_CODE_LENGTH));
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \App\Invitations\Generator\InvitationGeneratorInterface::generate()
     */
    public function generate(int $count): array
    {
        $this->validateCount($count);
        
        $iteration = 0;
        $invitation = new Invitation($this->em);
        
        while (count($this->invitations) < $count && $iteration <= self::MAX_INCREASE_LIMIT) {
            try {
                $invitationEntity = $invitation->add($this->getUniqueCode(), $this->userId);
                
                if (null !== $invitationEntity){
                    $this->invitations[] = $invitationEntity->getCode();
                }
                
                $iteration++;
                
            } catch (InvitationException $e) {
                #TODO: here we can make rollback if some invitaions was created or we can return just part of it
                throw new InvitationGeneratorException('Error while generatings invite codes');
            } 
        }
        
        return $this->invitations;
    }
    
    private function validateCount(int $count): bool
    {
        if ($count > self::MAX_GENERATED_CODES_COUNT) {
            throw new InvitationGeneratorTooBigNumberException('Number is to big');
        }
        
        return true;
    }
}