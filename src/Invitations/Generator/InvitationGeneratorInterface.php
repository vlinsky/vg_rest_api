<?php
namespace App\Invitations\Generator;

/**
 * InvitationGeneratorInterface
 *
 */
interface InvitationGeneratorInterface
{
    /**
     * Generate invitatin codes
     * 
     * generate invitatin codes and return its array
     * 
     * @param int $count
     * @return array
     */
    public function generate(int $count): array;
    
    /**
     * Generate unique code string
     *
     * @return string
     */
    public function getUniqueCode(): string;
}