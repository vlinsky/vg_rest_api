<?php
namespace App\Invitations\Generator;

/**
 * InvitationGeneratorTooBigNumberException class
 *
 * throws if requested number of code is too big
 */
class InvitationGeneratorTooBigNumberException extends InvitationGeneratorException
{
    
}