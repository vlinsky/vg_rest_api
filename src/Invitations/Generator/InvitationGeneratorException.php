<?php
namespace App\Invitations\Generator;

/**
 * InvitationGeneratorException
 * 
 * throws then InvitationGenerator fails
 */
class InvitationGeneratorException extends \Exception
{
    
}