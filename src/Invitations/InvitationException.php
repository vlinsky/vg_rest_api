<?php
namespace App\Invitations;

/**
 * Invitation Exception 
 * 
 * throws then error occurn while working with Invitation object
 */
class InvitationException extends \Exception
{
    
}