<?php
namespace App\Invitations;

/**
 * InvitationList interface
 *
 */
interface InvitationListInterface
{
    /**
     * Get invitation by user id
     * 
     * @param mixed $userId
     * @return array
     */
    public function getByUserId($userId): array;
}