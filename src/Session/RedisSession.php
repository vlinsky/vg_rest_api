<?php
namespace App\Session;

use App\Config\Config;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use App\Security\Token;
use App\Security\TokenExpiredException;

/**
 * Redis session storage class
 */
class RedisSession implements SessionInterface
{
    private $client;
    
    public function __construct()
    {
        $this->client = RedisAdapter::createConnection(Config::getRedisConnectionString());
    }
    
    /**
     * Set session data to redis storage
     * 
     * @param string $key
     * @param string $value
     * 
     * @return array|void|NULL[]
     */
    public function set(string $key, string $value)
    {
        $key = Config::getRedisSessionNamespace().$key;
        return $this->client->transaction()->set($key, $value)->expire($key, Config::getRedisSessionTimeout())->execute();
    }
    
    /**
     * Get session data from redis storage
     * 
     * @param string $key
     * @throws TokenExpiredException
     * @return NULL
     */
    public function get(string $key =  null)
    {
        $key = Config::getRedisSessionNamespace().$key;
        $res = $this->client->transaction()->get($key)->expire($key, Config::getRedisSessionTimeout())->execute();
        
        if (empty($res[0])) {
            throw new TokenExpiredException('token expired');
        }
        
        return $res[0];
    }
    
    /**
     * Generate session uniq key
     * 
     * @param Token $token - UriSafeTokenGenerator object
     * @return string
     */
    public function generateToken(Token $token)
    {
        return $token->generateToken();
    }
}