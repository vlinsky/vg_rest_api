<?php
namespace App\Session;

use Symfony\Component\Security\Csrf\TokenGenerator\UriSafeTokenGenerator;

/**
 * SessionInterface
 */
interface SessionInterface
{
    /**
     * Set key with the value
     * 
     * @param string $key
     * @param string $value
     */
    public function set(string $key, string $value);
    
    /**
     * Get value for key
     * 
     * @param string $key
     */
    public function get(string $key);
}