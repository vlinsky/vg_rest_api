<?php

namespace App\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $passwd;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $inviteId;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateAdded;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPasswd(): ?string
    {
        return $this->passwd;
    }

    public function setPasswd(string $passwd): self
    {
        $this->passwd = $passwd;

        return $this;
    }

    public function getInviteId(): ?int
    {
        return $this->inviteId;
    }

    public function setInviteId(?int $inviteId): self
    {
        $this->inviteId = $inviteId;

        return $this;
    }

    public function getDateAdded(): ?\DateTimeInterface
    {
        return $this->dateAdded;
    }

    public function setDateAdded(\DateTimeInterface $dateAdded): self
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }
    
    public function getPassword(): ?string
    {
        return $this->passwd;
    }

    public function eraseCredentials()
    {
        return true;
    }

    public function getSalt()
    {
        return null;
    }

    public function getRoles()
    {
        
    }

    public function getUsername(): ?string
    {
        return $this->email;
    }

}
