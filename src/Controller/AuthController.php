<?php
 namespace App\Controller;
 
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Http\Response;
use App\Session\RedisSession;
use App\User\UserException;
use App\User\UserManager;
use App\Security\Token;

class AuthController extends AbstractController
{
    /**
     * Auth user with login and password
     * 
     * @Route("/auth/", name="auth", methods={"POST","GET"})
     * 
     * @param Request $request
     * @param RedisSession $rsession
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $encoder
     * @param ValidatorInterface $validator
     * @param LoggerInterface $logger
     * @return \App\Http\Response
     */
    public function authUser(Request $request, RedisSession $rsession, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder, ValidatorInterface $validator, LoggerInterface $logger)
     {
         try {
             $email = $request->get("email");
             $passwd = $request->get("passwd");
             
             $userManager = new UserManager($em, $encoder, $validator);
             $user = $userManager->checkAuth($email, $passwd);
             
             $userData = array(
                 'id' => $user->getId(),
                 'email' => $user->getEmail(),
                 'dateAdded' => $user->getDateAdded()
             );
             
             $token = $rsession->generateToken(new Token());
             $rsession->set($token, json_encode($userData));
             
             return new Response(array('status' => 1, 'token' => $token));
         } catch (UserException $e) {
             $logger->error($e->getMessage());
             $logger->error(json_encode($e->getTrace()));
             return new Response(array('status' => 0, 'error' => $e->getMessage()), 400);
         } catch (\Exception $e) {
             $logger->error($e->getMessage());
             $logger->error(json_encode($e->getTrace()));
             return new Response(array('status' => 0, 'error' => 'Error occurs'), 400);
         }
     }
 }