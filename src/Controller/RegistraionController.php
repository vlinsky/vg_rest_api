<?php
namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Http\Response;
use App\Invitations\InvitationException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Entity\User as UserEntity;
use App\User\UserException;
use App\User\UserManager;

class RegistraionController extends AbstractController
{   
    /**
     * Add new user
     * 
     * @Route("/user/add/", name="user_add", methods={"POST","GET"})
     * 
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $encoder
     * @param ValidatorInterface $validator
     * @param LoggerInterface $logger
     * @return \App\Http\Response
     */
    public function add(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder, ValidatorInterface $validator, LoggerInterface $logger)
    {
        try {
            $email = $request->get("email");
            $passwd = $request->get("passwd");
            $invitationCode = $request->get("icode");
            
            $userManager = new UserManager($em, $encoder, $validator);
            
            $userManager->add($email, $passwd, $invitationCode);
            
            return new Response(array('status' => 1));
        } catch (UserException $e) {
            $logger->error($e->getMessage());
            $logger->error(json_encode($e->getTrace()));
            return new Response(array('status' => 0, 'error' => $e->getMessage()), 400);
        } catch (InvitationException $e) {
            $logger->error($e->getMessage());
            $logger->error(json_encode($e->getTrace()));
            return new Response(array('status' => 0, 'error' => $e->getMessage()), 400);
        } catch (\Exception $e) {
            $logger->error($e->getMessage());
            $logger->error(json_encode($e->getTrace()));
            return new Response(array('status' => 0, 'error' => 'Error occurs'), 400);
        }
    }
}