<?php
namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Http\Response;
use App\Session\RedisSession;
use App\Security\TokenExpiredException;
use App\Invitations\Generator\InvitationGenerator;
use App\Invitations\Generator\InvitationGeneratorException;

class InvitationGeneratorController extends AbstractController
{
    /**
     * Generate invite codes
     * 
     * @Route("/invitaion/generate/", name="generate_invitaions", methods={"GET"})
     * 
     * @param Request $request
     * @param RedisSession $rsession
     * @param EntityManagerInterface $em
     * @param LoggerInterface $logger
     * @return \App\Http\Response
     */
    public function generate(Request $request, RedisSession $rsession, EntityManagerInterface $em, LoggerInterface $logger)
    {
        try {
            $token = $request->get("token");
            $number = $request->get("number");
           
            $userData = json_decode($rsession->get($token));
            
            $generator = new InvitationGenerator($userData->id, $em);
            $codesList = $generator->generate((int)$number);
            
            return new Response(array('status' => 1, 'codes' => $codesList));
        } catch (TokenExpiredException $e){
            $logger->error($e->getMessage());
            $logger->error(json_encode($e->getTrace()));
            return new Response(array('status' => 0, 'error' => $e->getMessage()), 401);
        } catch (InvitationGeneratorException $e){
            $logger->error($e->getMessage());
            $logger->error(json_encode($e->getTrace()));
            return new Response(array('status' => 0, 'error' => $e->getMessage()), 401);
        } catch (\Exception $e) {
            $logger->error($e->getMessage());
            $logger->error(json_encode($e->getTrace()));
            return new Response(array('status' => 0, 'error' => 'Error occurs'), 400);
        }
    }
}