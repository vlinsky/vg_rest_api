<?php
namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Http\Response;
use App\Session\RedisSession;
use App\Security\TokenExpiredException;
use App\Invitations\InvitationList;
use App\Invitations\InvitationListException;

class InvitationListController extends AbstractController
{
    /**
     * Get user invite codes list
     * 
     * @Route("/invitaion/list/", name="inivitation_list", methods={"GET"})
     * 
     * @param Request $request
     * @param RedisSession $rsession
     * @param EntityManagerInterface $em
     * @param LoggerInterface $logger
     * @return \App\Http\Response
     */
    public function getList(Request $request, RedisSession $rsession, EntityManagerInterface $em, LoggerInterface $logger)
    {
        try {
            $token = $request->get("token");
            
            $userData = json_decode($rsession->get($token));
            
            $invitationList = new InvitationList($em);
            $codesList = $invitationList->getByUserId($userData->id);
            
            return new Response(array('status' => 1, 'codes' => $codesList));
        } catch (TokenExpiredException $e){
            $logger->error($e->getMessage());
            $logger->error(json_encode($e->getTrace()));
            return new Response(array('status' => 0, 'error' => $e->getMessage()), 401);
        } catch (InvitationListException $e){
            $logger->error($e->getMessage());
            $logger->error(json_encode($e->getTrace()));
            return new Response(array('status' => 0, 'error' => $e->getMessage()), 401);
        } catch (\Exception $e) {
            $logger->error($e->getMessage());
            $logger->error(json_encode($e->getTrace()));
            return new Response(array('status' => 0, 'error' => 'Error occurs'), 400);
        }
    }
    
    /**
     * Get invitation short statistic
     * 
     * get count of used and unused invitations
     * 
     * @Route("/invitaion/statistic/", name="inivitation_statistic", methods={"GET"})
     * 
     * @param Request $request
     * @param RedisSession $rsession
     * @param EntityManagerInterface $em
     * @param LoggerInterface $logger
     * @return \App\Http\Response
     */
    public function getListStatistic(Request $request, RedisSession $rsession, EntityManagerInterface $em, LoggerInterface $logger)
    {
        try {
            $token = $request->get("token");
            
            $userData = json_decode($rsession->get($token));
            
            $invitationList = new InvitationList($em);
            $statistic = $invitationList->getShortStatistic($userData->id);
            
            return new Response(array('status' => 1, 'statistic' => $statistic));
        } catch (TokenExpiredException $e){
            $logger->error($e->getMessage());
            $logger->error(json_encode($e->getTrace()));
            return new Response(array('status' => 0, 'error' => $e->getMessage()), 401);
        } catch (\Exception $e) {
            $logger->error($e->getMessage());
            $logger->error(json_encode($e->getTrace()));
            return new Response(array('status' => 0, 'error' => 'Error occurs'), 400);
        }
    }
}