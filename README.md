# VG REST API

####Installation instruction  
  
git clone https://gitlab.com/vlinsky/vg_rest_api.git  
cd vg_rest_api  
composer install  

####Configuration instruction  
  
change DATABASE_URL and REDIS_URL in .env file according to your settings  
  
####Usage  
  
User registration : /user/add/  
method : POST, GET  
params :  
  email, passwd, icode if present  
return : json  
success example : {"status":1}  
error example : {"status":0,"error":"This value is not a valid email address."}  
  
  
User login : /auth/  
method : POST, GET  
params :  
  email, passwd  
return : json  
success example : {"status":1,"token":"SadN2ALLwnlpqHirovLNm9OIBrKHoZCjnWcG1uyXdO8"}  
error example : {"status":0,"error":"UserManager not found. Please check email or password"}  
  
  
Invitation generator : /invitaion/generate/  
method : GET  
params :  
  token, number - cannot be more then 10   
return : json  
success example : {"status":1,"codes":["4AA800D846","CD9E4722D1"]}  
error example : {"status":0,"error":"Number is to big"}  
  
  
Invitation code list : /invitaion/list/  
method : GET  
params :  
  token  
return : json  
success example : {"status":1,"codes":[{"id":8,"code":"80EF18937E","userId":33,"dateAdded":  {"date":"2019-10-10 09:51:17.000000","timezone_type":3,"timezone":"UTC"},"dateUsed":null},{"id":  9,"code":"E174F2A013","userId":33,"dateAdded":{"date":"2019-10-10 09:51:17.000000","timezone_type":  3,"timezone":"UTC"},"dateUsed":null}]}  
error example : {"status":0,"error":"Error geting invite codes"}  

Invitation short statistic : /invitaion/statistic/  
method : GET  
params :  
  token  
return : json  
success example : {"status":1,"statistic":{"registered":0,"invited":2}}  
error example : {"status":0,"error":"Error geting invite codes statistic"}  

